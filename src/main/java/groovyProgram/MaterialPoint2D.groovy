package groovyProgram;

class MaterialPoint2D extends Point2D {

    private double x
    private double y
    private double mass


    MaterialPoint2D(double x , double y , double mass){
        super(x,y)
        this.x = x
        this.y = y
        this.mass = mass
    }

    def getMass() {
        mass
    }

    String toString(){
        return "x: " + x + ", y: " + y + ", mass:" + mass
    }

}
