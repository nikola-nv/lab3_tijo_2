package javaProgram;

public class Calculations {
    public static Point2D positionGeometricCenter(Point2D[] points){
        float sumOfX = 0;
        float sumOfY = 0;
        int count = 0;

        for(Point2D point : points){
            sumOfX += point.getX();
            sumOfY += point.getY();
            count++;
        }
        return new Point2D(sumOfX/count,sumOfY/count);
    }

    public static Point2D positionCenterOfMass(MaterialPoint2D[] materialPoints){
        float sumOfX = 0;
        float sumOfY = 0;
        float sumOfMass = 0;

        for(MaterialPoint2D materialPoint2D : materialPoints){
            sumOfX += materialPoint2D.getX() * materialPoint2D.getMass();
            sumOfY += materialPoint2D.getY() * materialPoint2D.getMass();

            sumOfMass+= materialPoint2D.getMass();
        }
        return new MaterialPoint2D(sumOfX/sumOfMass,sumOfY/sumOfMass,sumOfMass);
    }
}
