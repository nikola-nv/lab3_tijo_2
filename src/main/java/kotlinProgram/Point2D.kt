package kotlinProgram

open class Point2D{
    private var x : Double
    private var y : Double


    constructor(x : Double , y : Double){
        this.x = x
        this.y = y
    }

    fun getX() : Double{
        return x
    }

    fun getY() : Double {
        return y
    }


    override fun toString() : String {
        return "x: " + x + ", y: " + y
    }
}
