package kotlinProgram

const val POINTS : Int = 2
    fun main(args : Array<String>){
        val points = arrayOfNulls<Point2D>(POINTS)
        val materialPoints = arrayOfNulls<MaterialPoint2D>(POINTS)

        var calculations : Calculations = Calculations()

        points.set (0, Point2D(0.0, 0.0))
        points.set (1, Point2D(10.0, 10.0))

        materialPoints.set (0, MaterialPoint2D(0.0, 0.0, 10))
        materialPoints.set (1, MaterialPoint2D(10.0, 10.0, 100))

        var geometricCenter : Point2D = calculations.positionGeometricCenter(points)
        var massCenter : Point2D = calculations.positionCenterOfMass(materialPoints)

        println("Polozenie srodka masy: " + massCenter);
        println("Polozenie srodka geometrycznego: " + geometricCenter);
    }

