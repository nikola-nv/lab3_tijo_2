package javaProgram;

public class MaterialPoint2D extends Point2D{

    private double x;
    private double y;
    private double mass;


    public MaterialPoint2D(double x , double y , double mass){
        super(x,y);
        this.x = x;
        this.y = y;
        this.mass = mass;
    }

    public double getMass() {
        return mass;
    }

    @Override
    public String toString(){
        return "x: " + x + ", y: " + y + ", mass:" + mass;
    }

}
