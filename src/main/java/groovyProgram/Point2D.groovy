package groovyProgram;

class Point2D{
    private double x
    private double y


    Point2D(double x , double y){
        this.x = x
        this.y = y
    }

    def getX() {
        x
    }

    def getY() {
        y
    }

    String toString(){
        return "x: " + x + ", y: " + y
    }
}
