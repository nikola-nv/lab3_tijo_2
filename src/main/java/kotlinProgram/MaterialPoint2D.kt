package kotlinProgram



class MaterialPoint2D: Point2D {


    private var mass : Int


    constructor(x : Double , y : Double , mass : Int) : super(x,y) {
        this.mass = mass
    }



    fun getMass(): Int {
        return mass
    }


    override fun toString(): String{
        return "x: ${getX()} , y: ${getY()} , mass:$mass"
    }

}
