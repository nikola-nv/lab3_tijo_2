package groovyProgram;

class Calculations {
    static def positionGeometricCenter(Point2D[] points){
        float sumOfX = 0
        float sumOfY = 0
        int count = 0

        for(point in points){
            sumOfX += point.getX()
            sumOfY += point.getY()
            count++
        }
        new Point2D(sumOfX/count,sumOfY/count)
    }

    static def positionCenterOfMass(MaterialPoint2D[] materialPoints){
        float sumOfX = 0
        float sumOfY = 0
        float sumOfMass = 0

        for(materialPoint2D in materialPoints){
            sumOfX += materialPoint2D.getX() * materialPoint2D.getMass()
            sumOfY += materialPoint2D.getY() * materialPoint2D.getMass()

            sumOfMass+= materialPoint2D.getMass()
        }
        new MaterialPoint2D(sumOfX/sumOfMass,sumOfY/sumOfMass,sumOfMass)
    }
}
