package kotlinProgram;

class Calculations {
    fun positionGeometricCenter(points : Array<Point2D?>) : Point2D{
        var sumOfX : Double = 0.0
        var sumOfY : Double = 0.0
        var count : Int = 0

        for(point in points){
            sumOfX += point!!.getX()
            sumOfY += point!!.getY()
            count++
        }
        return Point2D(sumOfX/count,sumOfY/count)
    }

    fun positionCenterOfMass(materialPoints : Array<MaterialPoint2D?>) : Point2D{
        var sumOfX : Double = 0.0
        var sumOfY : Double = 0.0
        var sumOfMass : Int = 0

        for(materialPoint2D in materialPoints){
            sumOfX += materialPoint2D!!.getX() * materialPoint2D.getMass()
            sumOfY += materialPoint2D!!.getY() * materialPoint2D.getMass()

            sumOfMass = (materialPoint2D.getMass() + sumOfY).toInt()
        }
        return MaterialPoint2D(sumOfX/sumOfMass,sumOfY/sumOfMass,sumOfMass)
    }
}
